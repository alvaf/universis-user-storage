module.exports = function (api) {
    api.cache(false);
    return {
        "sourceMaps": "both",
        "presets": [
            [
                "@babel/preset-env",
                {
                    "targets": {
                        "node": "8.9.0"
                    }
                }
            ]
        ],
        "ignore": [
            "./node_modules/"
        ],
        "plugins": [
            [
                "@babel/plugin-proposal-decorators",
                {
                    "legacy": true
                }
            ],
            [
                "@babel/plugin-proposal-class-properties",
                {
                    "loose": true
                }
            ]
        ]
    };
};
