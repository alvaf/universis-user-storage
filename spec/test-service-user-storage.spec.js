import request from 'supertest';
import { UserStorageService,
    MongoUserStorageService,
    UserStorageAccessConfiguration} from '../modules/storage/src';
import {ExpressDataApplication} from '@themost/express';
import {getApplication, serveApplication, getToken, getServerAddress} from '@themost/test';

describe('UserStorageService endpoints', () => {

    let app;
    let token;
    beforeAll(done => {
        app = getApplication();
        /**
         * @type {ExpressDataApplication}
         */
        const application = app.get(ExpressDataApplication.name);
        // set mongo configuration
        // set mongo configuration
        application.getConfiguration().setSourceAt('settings/universis/storage/options', {
            "host": "localhost",
            "port": 27017,
            "database": "local_test"
        });
        // use service
        application.useStrategy(UserStorageService, MongoUserStorageService);
        // add user access
        application.useService(UserStorageAccessConfiguration);
        /**
         * @type {UserStorageAccessConfiguration}
         */
        const userStorageAccessConfiguration = application.getConfiguration().getStrategy(UserStorageAccessConfiguration);
        userStorageAccessConfiguration.elements.push({
            "scope": [
                "profile"
            ],
            "resource": "me/registrar/?",
            "access": [
                "read",
                "write"
            ]
        });
        serveApplication(app).then( liveServer => {
            const serverAddress = getServerAddress(liveServer);
            return getToken(serverAddress, 'alexis.rees@example.com', 'secret').then( value => {
                token = value;
                return done();
            });
        }).catch( err => {
            return done(err);
        });

    });

    it('POST /api/users/me/storage/set', async ()=> {
        // set user configuration
        let response = await request(app)
            .post('/api/users/me/storage/set')
            .set({
                'Authorization': `Bearer ${token.access_token}`
            })
            .send(
                {
                    key: "registrar",
                    value: {
                        "lastDepartment": "1200",
                        "lastAction": "/requests/active"
                    }
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.status).toBe(200);
        expect(response.body).toBeTruthy();
        // get user configuration
        response = await request(app)
            .post('/api/users/me/storage/get')
            .set({
                'Authorization': `Bearer ${token.access_token}`
            })
            .set('Accept', 'application/json')
            .send(
                {
                    key: "registrar"
                }
            );
        expect(response.status).toBe(200);
        expect(response.body).toBeTruthy();
        expect(response.body.value.lastDepartment).toBe("1200");
        // remove user configuration
        response = await request(app)
            .post('/api/users/me/storage/set')
            .set({
                'Authorization': `Bearer ${token.access_token}`
            })
            .send(
                {
                    key: "registrar",
                    value: null
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        // get user configuration again
        response = await request(app)
            .post('/api/users/me/storage/get')
            .set({
                'Authorization': `Bearer ${token.access_token}`
            })
            .set('Accept', 'application/json')
            .send(
                {
                    key: "registrar"
                }
            );
        expect(response.status).toBe(200);
        expect(response.body).toBeTruthy();
        expect(response.body.value).toBeFalsy();
    });

    it('POST /api/users/me/storage/get VALUE', async ()=> {
        // set user configuration
        await request(app)
            .post('/api/users/me/storage/set')
            .set({
                'Authorization': `Bearer ${token.access_token}`
            })
            .send(
                {
                    key: "registrar",
                    value: {
                        "lastDepartment": "1200",
                        "lastAction": "/requests/active"
                    }
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        let response = await request(app)
            .post('/api/users/me/storage/get')
            .set({
                'Authorization': `Bearer ${token.access_token}`
            })
            .set('Accept', 'application/json')
            .send(
                {
                    key: "registrar/lastDepartment"
                }
            );
        expect(response.status).toBe(200);
        expect(response.body).toBeTruthy();
        expect(response.body.value).toBe("1200");
        // remove user configuration
        response = await request(app)
            .post('/api/users/me/storage/set')
            .auth('admin', 'secret')
            .send(
                {
                    key: "registrar",
                    value: null
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
    });

    it('POST /api/users/me/storage/get ARRAY', async ()=> {
        // set user configuration
        await request(app)
            .post('/api/users/me/storage/set')
            .set({
                'Authorization': `Bearer ${token.access_token}`
            })
            .send(
                {
                    key: "registrar",
                    value: {
                        "lastDepartment": "1200",
                        "lastActions": [ "/requests/active", "/requests/list" ]
                    }
                }
            )
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        let response = await request(app)
            .post('/api/users/me/storage/get')
            .set({
                'Authorization': `Bearer ${token.access_token}`
            })
                .set('Accept', 'application/json')
            .send(
                {
                    key: "registrar/lastActions"
                }
            );
        expect(response.status).toBe(200);
        expect(response.body).toBeTruthy();
        expect(response.body.value).toBeInstanceOf(Array);
        expect(response.body.value[1]).toBe("/requests/list");
        // remove user configuration
        await request(app)
            .post('/api/users/me/storage/set')
            .set({
                'Authorization': `Bearer ${token.access_token}`
            })
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .send(
                {
                    key: "registrar",
                    value: null
                }
            )

    });

});
